package org.batili.servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.batili.model.Commune;
import org.glassfish.api.container.RequestDispatcher;

@WebServlet(urlPatterns = "/commune")
public class HelloWorldServlet extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		doPost(req, resp);
		
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		// TODO Auto-generated method stub
	Commune commune =new Commune("93800","Epinay-Sur-Seine",12);
	req.setAttribute("commune", commune);
	RequestDispatcher rd= (RequestDispatcher) req.getRequestDispatcher("commune.jsp");
	((javax.servlet.RequestDispatcher) rd).forward(req,resp);
	}
	

}
