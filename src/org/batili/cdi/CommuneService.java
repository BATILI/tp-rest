package org.batili.cdi;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.batili.model.Commune;

public class CommuneService {
	@PersistenceContext(unitName = "tp-javaee")
	private EntityManager em;
	
	public Commune FindById(String codePostal)
	{
		return em.find(Commune.class, codePostal);
		
	}

}
