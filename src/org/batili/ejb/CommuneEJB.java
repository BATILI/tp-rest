package org.batili.ejb;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.batili.model.Commune;

@Stateless
public class CommuneEJB {
	@PersistenceContext(unitName = "tp_jpa")
	private EntityManager em;
	public Commune FindById(String codePostal)
	{
		return em.find(Commune.class, codePostal);
		
	}

}
