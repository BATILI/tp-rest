package org.batili.rest;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.batili.cdi.CommuneService;
import org.batili.model.Commune;

@Path("commune")

public class PlayWithCommunes {
	@Inject
	private CommuneService communeService;
	
	@GET  @Path("{code-postal}")
	@Produces(MediaType.TEXT_XML)
	public Response FindById(@PathParam("code-postal") String codePostal)
	{
		Commune commune =communeService.FindById(codePostal);
		if(commune==null) {
			return Response.status(Status.NOT_FOUND).build();
		}else {
		
		return Response
				.ok(commune)
				.build();
	}
	}

}
