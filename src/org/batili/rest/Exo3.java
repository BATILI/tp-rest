package org.batili.rest;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.batili.model.Commune;

@Path("commune")
public class Exo3 {
	@GET @Path("/{name}/{codepostal}") 
	@Produces(MediaType.TEXT_XML)
	public Response commune(@PathParam("name") String name,@PathParam("codepostal") String codepostal) {
		
		Commune com=new Commune(name,codepostal, 10000);
		return Response.ok().entity(com).build();
}
}