package org.batili.model;

import java.io.Serializable;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;
import java.util.stream.Collectors;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;


@Entity
public class Pays implements Serializable{

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	public int id;
	
	@Column(length = 80, nullable = false)
	public String nom;

	@OneToMany(mappedBy = "pays")
	public Map<String,Departement> departements = new HashMap<>();
		
	public Pays(String nom) {
		this.nom = nom;
	}
	public Pays() {
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public boolean addDepartement(Departement dep) {
		
		Departement d = this.departements.put(dep.getCodeDepartement(),dep);
		if(d != null)
		return true;
		else return false;
	}
	
	public Departement getDepartementByCodePostal(String codePostal) {
		Departement retour = new Departement();
		String dep = codePostal.substring(0, 2);
 	for( Entry<String, Departement> entry : departements.entrySet()) {
 		if(entry.getKey().equals(dep)) {
 			retour = entry.getValue();
 		}
 	}
		
		return retour;
	}
	
	@Override
	public String toString() {
		return " "+ nom  ;
	}
	
}
