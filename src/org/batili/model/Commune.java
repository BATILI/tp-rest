package org.batili.model;
import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.persistence.Table;

@Entity(name = "Commune")
public class Commune implements Serializable{
	
	@Id
	public String codepostal;
	
	@Column(length = 80, nullable = false)
	public String nom;
	
	@Column
	public long population;
	
	@OneToOne(cascade = CascadeType.PERSIST)
	public maire maire;
	
	public maire getMaire() {
		return maire;
	}

	public void setMaire(maire maire) {
		this.maire = maire;
	}

	@ManyToOne(cascade = CascadeType.PERSIST)
	public Departement departement;
	
	
	
	public Departement getDepartement() {
		return departement;
	}

	public void setDepartement(Departement departement) {
		this.departement = departement;
	}

	public Commune() {
	}
	
	public Commune(String codepostal, String nom,long population) {

		this.codepostal = codepostal;
		this.nom = nom;
		this.population = population;
	}
	public String getCodepostal() {
		return codepostal;
	}
	public void setCodepostal(String codepostal) {
		this.codepostal = codepostal;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}

	@Override
	public String toString() {
		return "Commune [codepostal=" + codepostal + ", nom=" + nom + ", maire=" + maire + "]";
	}
	
	public static EntityManagerFactory emf = Persistence.createEntityManagerFactory("TP JPA");
	public static EntityManager entitymanager = emf.createEntityManager();
	
	public List countPopulationUnder(long popmin) {
		
		String sql = "select nom, population from Commune where population < :popmin ";

		Query query = entitymanager.createQuery(sql);
		query.setParameter("popmin", popmin);
		List s =  query.getResultList();
		return s;
	}
	
	public long populationRatioUnderAvg() {	
		String sql = "select count(c)*100 / (select count(c1) from Commune c1) "
				+ "from Commune c where population < (select avg(c.population) from Commune c) ";

		Query query = entitymanager.createQuery(sql);
		long s =(long) query.getSingleResult();
		return s;
	}
}
