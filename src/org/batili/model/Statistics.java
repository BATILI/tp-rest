package org.batili.model;

public class Statistics {

	public long countCities;
	public long countPeople;
	public double average;

	public Statistics(long countCities, long countPeople, double average) {
		this.countCities = countCities;
		this.countPeople = countPeople;
		this.average = average;
	}
	
	public long getCountCities() {
		return countCities;
	}
	public void setCountCities(long countCities) {
		this.countCities = countCities;
	}
	public long getCountPeople() {
		return countPeople;
	}
	public void setCountPeople(long countPeople) {
		this.countPeople = countPeople;
	}
	public double getAverage() {
		return average;
	}
	public void setAverage(double average) {
		this.average = average;
	}

	@Override
	public String toString() {
		return "Statistics [countCities=" + countCities + ", countPeople=" + countPeople + ", average=" + average + "]";
	}
	
	
	
}
