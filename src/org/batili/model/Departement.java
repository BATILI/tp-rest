package org.batili.model;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Persistence;
import javax.persistence.Query;

@Entity(name = "Departement")
public class Departement implements Serializable {

	@Id
	public String codeDepartement;

	@Column(length = 80, nullable = false)
	public String nom;

	@OneToMany(mappedBy = "departement")
	public Set<Commune> communes = new HashSet<>();

	@ManyToOne(cascade = CascadeType.PERSIST)
	public Pays pays;

	public Pays getPays() {
		return pays;
	}

	public void setPays(Pays pays) {
		this.pays = pays;
	}

	public Departement() {
	}

	public Departement(String codeDepartement, String nom) {
		this.codeDepartement = codeDepartement;
		this.nom = nom;
	}

	public String getCodeDepartement() {
		return codeDepartement;
	}

	public void setCodeDepartement(String codeDepartement) {
		this.codeDepartement = codeDepartement;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public boolean addCommune(Commune commune) {
		return this.communes.add(commune);
	}

	@Override
	public String toString() {
		return "Departement [codeDepartement=" + codeDepartement + ", nom=" + nom + ", pays=" + pays + "]";
	}

	public static EntityManagerFactory emf = Persistence.createEntityManagerFactory("TP JPA");
	public static EntityManager entitymanager = emf.createEntityManager();

	public Departement getByName(String name) {
		String sql = "select d from Departement d where d.nom = :nom";

		Query query = entitymanager.createQuery(sql);
		query.setParameter("nom", name);
		Departement d = (Departement) query.getSingleResult();

		return d;
	}

	public long getNumberOfCities() {
		String sql = "select count(c) from Commune c";
		Query query = entitymanager.createQuery(sql);
		long d = (long) query.getSingleResult();

		return d;
	}

	public maire getOldestMayor() {
		String sql = "select m from maire m where date_naissance = (select min(date_naissance) from maire)";
		Query query = entitymanager.createQuery(sql);
		maire m = (maire) query.getSingleResult();

		return m;
	}

	public long countWomanMayorRatio(String name) {
		String sql = "select count(c.maire)*100/(select count(c1.maire) from Departement d1,"
				+ " in(d1.communes) c1 where d1.nom = :nom AND c1.departement = d1)"
				+ " from Departement d, in(d.communes) c where (c.maire.civility = 'MME' OR "
				+ "c.maire.civility = 'MLLE') AND d.nom = :nom AND c.departement = d";

		Query query = entitymanager.createQuery(sql);
		query.setParameter("nom", name);
		long count = (long) query.getSingleResult();

		return count;
	}

	public Statistics statistics() {
		String sql = "select new org.MOUHSINI.Serie3.exo5.model.Statistics(count(c), sum(c.population)"
				+ ", avg(c.population)) from Departement d, in(d.communes) c where d.nom = :nom";

		Query query = entitymanager.createQuery(sql);
		query.setParameter("nom", this.nom);
		Statistics s = (Statistics) query.getSingleResult();
		return s;
	}

}
