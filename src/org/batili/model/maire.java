package org.batili.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity(name = "maire")
public class maire {
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	public int id;
	
	@OneToOne(mappedBy = "maire")
	public Commune commune;
	
	public Commune getCommune() {
		return commune;
	}

	public void setCommune(Commune commune) {
		this.commune = commune;
	}

	@Column(length = 80, nullable = false)
	public String nom;
	
	@Column(length = 80, nullable = false)
	public String prenom;
	
	@Column(length=8)
	@Enumerated(EnumType.STRING)
	public Civility civility;
	
	@Temporal(TemporalType.TIMESTAMP)
    public Date date_naissance;

	public maire() {
	}
	
	public maire(String nom, String prenom, Civility civility, Date date_naissance) {
		this.nom = nom;
		this.prenom = prenom;
		this.civility = civility;
		this.date_naissance = date_naissance;
	}



	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public Civility getCivility() {
		return civility;
	}

	public void setCivility(Civility civility) {
		this.civility = civility;
	}

	public Date getDate_naissance() {
		return date_naissance;
	}

	public void setDate_naissance(Date date_naissance) {
		this.date_naissance = date_naissance;
	}

	@Override
	public String toString() {
		return "maire [id=" + id + ", nom=" + nom + ", prenom=" + prenom + ", civility=" + civility
				+ ", date_naissance=" + date_naissance + "]";
	}
	

}
